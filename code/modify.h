#ifndef MODIFY_H
#define MODIFY_H

#include <QWidget>
#include <QMessageBox>
#include <QString>
namespace Ui {
class Modify;
}

class Modify : public QWidget
{
    Q_OBJECT

public:
    explicit Modify(QWidget *parent = nullptr);
    ~Modify();

signals:
    void backMenu();

private slots:
    void on_backButton_clicked();

    void on_modifyButton_clicked();

private:
    Ui::Modify *ui;
};

#endif // MODIFY_H
