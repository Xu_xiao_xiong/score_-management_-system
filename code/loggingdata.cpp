#include "loggingdata.h"
#include "ui_loggingdata.h"

LoggingData::LoggingData(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoggingData)
{
    ui->setupUi(this);
    //改变界面名称
    this->setWindowTitle("录入学生成绩");
}

LoggingData::~LoggingData()
{
    delete ui;
}

void LoggingData::on_backButton_clicked()//返回主菜单的槽函数
{
    emit this->backMenu();
}

void LoggingData::on_singleLoggingButton_clicked()
{
    bool all_digit=true;//判断输入的成绩是否全部为数字
    QString name=ui->stu_name->text();
    QString id=ui->stu_id->text();
    size_t chi_length=ui->stu_chinese->text().length();
    size_t math_length=ui->stu_math->text().length();
    size_t eng_length=ui->stu_english->text().length();
    size_t com_length=ui->stu_comprehension->text().length();
    for(size_t i=0;i<chi_length;i++)
    {
       if(! ui->stu_chinese->text().at(i).isDigit())
       {
           all_digit=false;
           break;
       }
    }
    if(all_digit)
    {
        for(size_t i=0;i<math_length;i++)
        {

            if(!ui->stu_math->text().at(i).isDigit())
            {
            all_digit=false;
            break;
            }
        }
    }
    if(all_digit)
    {
    for(size_t i=0;i<eng_length;i++)
    {

        if(!ui->stu_english->text().at(i).isDigit())
        {
            all_digit=false;
            break;
        }
    }
    }
    if(all_digit)
    {
        for(size_t i=0;i<com_length;i++)
        {

            if(!ui->stu_comprehension->text().at(i).isDigit())
            {
            all_digit=false;
            break;
            }
        }
    }
    if(!all_digit)
    {
        QMessageBox::warning(this,"输入提示","输入有误！");
        return;
    }//异常处理1：输入分数非数字
    unsigned chinese=ui->stu_chinese->text().toUInt();
    unsigned math=ui->stu_math->text().toUInt();
    unsigned english=ui->stu_english->text().toUInt();
    unsigned comprehension=ui->stu_comprehension->text().toUInt();

    if(chinese>150||math>150||english>150||comprehension>300)
    {
        QMessageBox::warning(this,"输入提示","输入有误！");
        return;
    }//异常处理2：输入分数超标

    //在此处将读取的数据写入数据库
    //codes here...


    //数据录入的异常处理
    //codes here...

    //如果录入成功了
    //条件判断代码
    QMessageBox::information(this,"录入信息","录入成功");

}

void LoggingData::on_bulkImportButton_clicked()
{
    QString filePath=ui->file_path->text();//获取
}
