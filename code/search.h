#ifndef SEARCH_H
#define SEARCH_H

#include <QWidget>
#include <QMessageBox>
#include <QPainter>
#include <QString>
#include "student.h"
namespace Ui {
class Search;
}

class Search : public QWidget
{
    Q_OBJECT

public:
    explicit Search(QWidget *parent = nullptr);
    ~Search();

signals:
    void backMenu();//回到主菜单的信号
private slots:
    void on_backButton_clicked();

    void on_searchButton_clicked();

private:
    Ui::Search *ui;
    // virtual void paintEvent(QPaintEvent *event);//绘制函数
};

#endif // SEARCH_H
