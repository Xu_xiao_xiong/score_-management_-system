#ifndef REGISTER_H
#define REGISTER_H

#include <QWidget>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QString>
#include <QLineEdit>

const QString filename="D:\\qtCode\\widget1\\infoOfAdmin.txt";//注册人员的信息的文件存放路径

namespace Ui {
class Register;
}

class Register : public QWidget
{
    Q_OBJECT

public:
    explicit Register(QWidget *parent = nullptr);
    ~Register();

signals:
   void backLogin();//回到登录界面的信号发送函数
   void backLogin_2();//注册按钮回到登陆界面的信号发送函数
private slots:
   void on_regisButton_clicked();

private:
    Ui::Register *ui;

};

#endif // REGISTER_H
