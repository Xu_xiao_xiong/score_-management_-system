#include "widget2.h"
#include "ui_widget2.h"

Widget2::Widget2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget2)
{
    ui->setupUi(this);
    this->ct=new scoreChart;//初始化成绩图表分析界面
    this->ld=new LoggingData;//初始化成绩录入界面
    this->sr=new Search;//初始化查询界面
    this->md=new Modify;//初始化修改界面
    //修改窗口名称
    this->setWindowTitle("菜单");
  //主菜单与成绩分析界面相联系
    connect(this->ct,&scoreChart::backMenu,[=](){
        this->ct->hide();
        this->show();
    });
    //主菜单与成绩录入界面相联系
    connect(this->ld,&LoggingData::backMenu,[=](){
        this->ld->hide();
        this->show();
    });
    //主菜单与成绩查询界面相联系
    connect(this->sr,&Search::backMenu,[=](){
        this->sr->hide();
        this->show();
    });
    //主菜单界面与成绩修改界面想恋兄
    connect(this->md,&Modify::backMenu,[=](){
        this->md->hide();
        this->show();
    });
}

Widget2::~Widget2()
{
    delete ui;
}

void Widget2::on_backButton_clicked()//返回登录界面
{
    emit this->backLogin();
}

void Widget2::on_scoreButton_clicked()//切换至成绩图表分析界面
{
    this->hide();
    this->ct->show();
}

void Widget2::on_loggingDataButton_clicked()//切换至录入学生成绩界面
{
    this->hide();
    this->ld->show();
}

void Widget2::on_searchButton_clicked()
{
    this->hide();
    this->sr->show();
}

void Widget2::on_modifyButton_clicked()
{
    this->hide();
    this->md->show();
}
