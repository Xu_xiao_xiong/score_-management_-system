#ifndef SCORECHART_H
#define SCORECHART_H

#include <QWidget>
#include <QPainter>
#include <QVector>

namespace Ui {
class scoreChart;
}

class scoreChart : public QWidget
{
    Q_OBJECT

public:
    explicit scoreChart(QWidget *parent = nullptr);
    ~scoreChart();

signals:
    void backMenu();//回到主菜单的信号
private slots:
    void on_backButton_clicked();

    void on_chineseButton_clicked();

    void on_mathButton_clicked();

    void on_englishButton_clicked();

    void on_comprehensionButton_clicked();

private:
    Ui::scoreChart *ui;
    virtual void paintEvent(QPaintEvent *event);
    enum Subject{None,Chinese,Math,English,Comprehension};//自定义一个枚举类型，包含各科成绩
    Subject currentSubject=None;//当前展示的科目成绩
};

#endif // SCORECHART_H
