#ifndef LOGGINGDATA_H
#define LOGGINGDATA_H

#include <QWidget>
#include <QMessageBox>
#include "student.h"
namespace Ui {
class LoggingData;
}

class LoggingData : public QWidget
{
    Q_OBJECT

public:
    explicit LoggingData(QWidget *parent = nullptr);
    ~LoggingData();

signals:
    void backMenu();
private slots:
    void on_backButton_clicked();

    void on_singleLoggingButton_clicked();

    void on_bulkImportButton_clicked();

private:

    Ui::LoggingData *ui;
};

#endif // LOGGINGDATA_H
