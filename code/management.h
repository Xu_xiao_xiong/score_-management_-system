#pragma once//防止头文件重复包含
#include<iostream>
using namespace std;
#include"worker.h"
#include<fstream>
#define filename "worker.txt"

class management
{
public:
	bool m_fileisempty;//判断文件是否为空

	int get_num();//获取人数

	int m_num;//记录职工人数

	worker** m_array;//职工数组指针

	management();//construct

	void show_menu();//展示菜单

	void exitsystem();//退出系统

	void add();//添加人员

	void save();//保存文件

	void init_worker();//初始化员工

	void show();//展示人员信息

	void del_worker();//删除员工信息

	void mod_worker();//修改员工信息 

	void find_worker();//查找员工信息

	void sort_worker();//对编号进行排序

	void clean_file();//清空文档
	 
	int isexistid(int id);//判断是否存在编号 如果存在返回编号

	string isexistname(string name);//判断是否存在名字 如果存在返回名字


	~management();//destroy

};

