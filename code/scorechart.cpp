#include "scorechart.h"
#include "ui_scorechart.h"

scoreChart::scoreChart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::scoreChart)
{
    ui->setupUi(this);
    this->setWindowTitle("成绩分析");
}



void scoreChart::paintEvent(QPaintEvent *event)
{
     QPainter painter(this);


     painter.setRenderHint(QPainter::Antialiasing);
 /*绘制坐标轴-------------------------------------*/
         // 定义坐标轴的起点和长度
         int startX = 400;
         int startY = height() -100;
         int axisLength = width() - 600;

         // 绘制坐标轴线
         painter.drawLine(startX, startY, startX + axisLength+20, startY);  // X 轴
         painter.drawLine(startX, startY, startX, 50);                   // Y 轴

         // 绘制 X 轴箭头
         painter.drawLine(startX + axisLength+20, startY,
                          startX + axisLength +10, startY + 5);
         painter.drawLine(startX + axisLength+20, startY,
                          startX + axisLength + 10, startY - 5);

         // 绘制 Y 轴箭头
         painter.drawLine(startX, 50, startX - 5, 60);
         painter.drawLine(startX, 50, startX + 5, 60);

         // 标记坐标轴刻度
         int numTicks = 10;  // 刻度数量
         double tickSpacing = static_cast<double>(axisLength) / numTicks;
         int tickValue = 0;

         for (int i = 0; i <= numTicks; ++i) {
             int x = startX + static_cast<int>(i * tickSpacing);
             int y = startY;

             painter.drawLine(x, y, x, y - 5);  // 绘制刻度线

             // 绘制刻度值
              QString label = QString::number(tickValue); // 格式化刻度值
             QRect rect = QRect(x - 25, y + 15, 50, 20+10);  // 文本框位置
             painter.drawText(rect, Qt::AlignHCenter, label);

             tickValue += 10;  // 增加刻度值
         }

         // 绘制坐标轴标签
         painter.drawText(QRect(startX + axisLength , startY + 10-10-10,90, 20+10+10), Qt::AlignHCenter, "成绩");
         painter.drawText(QRect(startX , 30+10, 70, 50), Qt::AlignHCenter, "百分比");
          /*绘制坐标轴-------------------------------------*/

        /* QColor color(255, 0, 0);  // 红色

         // 设置画笔和画刷

         painter.setBrush(color);
         for(int i=0;i<numTicks;i++)
         {
             int x = startX + static_cast<int>(i * tickSpacing);
             int y = startY;
             //于此处写上处理好的数据，绘制到界面上。
         painter.drawRect(x,y-(i+1)*10,tickSpacing,(i+1)*10);

         }*/
         switch (currentSubject)
         {
             case None:
                 break;
             case Chinese:
             {
                 QColor color(255,0,0);
                 painter.setBrush(color);
                 for(int i=0;i<numTicks;i++)
                 {
                     int x = startX + static_cast<int>(i * tickSpacing);
                     int y = startY;
                     //于此处写上处理好的数据，绘制到界面上。
                 painter.drawRect(x,y-(i+1)*10,tickSpacing,(i+1)*10);

                 }
             break;
             }
             case Math:
             {
             QColor color_2(0,0,255);
             painter.setBrush(color_2);
             for(int i=0;i<numTicks;i++)
                 {
                 int x = startX + static_cast<int>(i * tickSpacing);
                 int y = startY;
                 //于此处写上处理好的数据，绘制到界面上。
                 painter.drawRect(x,y-(i+1)*10,tickSpacing,(i+1)*10);
                 }
             break;
             }
         }

}

scoreChart::~scoreChart()
{
    delete ui;
}

void scoreChart::on_backButton_clicked()
{
    emit this->backMenu();
}

void scoreChart::on_chineseButton_clicked()
{
    currentSubject=Chinese;
    update();
}

void scoreChart::on_mathButton_clicked()
{
    currentSubject=Math;
    update();
}

void scoreChart::on_englishButton_clicked()
{
    currentSubject=English;
    update();
}

void scoreChart::on_comprehensionButton_clicked()
{
    currentSubject=Comprehension;
    update();
}
