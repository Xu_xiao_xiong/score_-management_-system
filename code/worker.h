#pragma once
#include<iostream>
using namespace std;
#include<string>
//人员抽象类
class worker
{
public:
	//显示个人信息
	virtual void showinfo()=0;
	//获取岗位名称
	virtual string getdepname() = 0;

	int ID;
	string name;
	string job;

	int total;//总分
	int chinese;//语文
	int math;//数学
	int english;//英语
	int comprehension;//综合
};

