#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "widget2.h"
#include "register.h"
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QLineEdit>
#include <QDebug>


QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();


private slots:

    void on_registerButton_clicked();//进入注册界面的槽函数

    void on_logInButton_clicked();

private:
    Widget2 *page=NULL;//这是进入菜单界面
    Register *reg=NULL;//注册界面
    Ui::Widget *ui;
};
#endif // WIDGET_H
