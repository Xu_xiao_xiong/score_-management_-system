#pragma once
#include "EventLoop.h"
#include "Buffer.h"
#include "Channel.h"
#include "Request.h"
#include "Response.h"
#include"MySqlConn.h"

//#define MSG_SEND_AUTO

class TcpConnection
{
public:
    TcpConnection(int fd, EventLoop* evloop,MySqlConn*mysql);
    ~TcpConnection();

    static int processRead(void* arg);
    static int processWrite(void* arg);
    static int destroy(void* arg);
private:
    string m_name;
    EventLoop* m_evLoop;
    Channel* m_channel;
    Buffer* m_readBuf;
    Buffer* m_writeBuf;
    // http 协议
    Request* m_request;
    Response* m_response;
    static MySqlConn* m_mysql;
};