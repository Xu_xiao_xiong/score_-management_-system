#pragma once
#include "Buffer.h"
#include <map>
#include <functional>
#include"json/json.h"
using namespace std;

// 定义状态码枚举
enum class StatusCode
{
    Unknown,
    //正常
    OK = 1,
    //失败
    Failure = 2,
    //无权限
    Illegal = 3,
    //数据重复
    Repeat=4,
    //密码错误
    WrongPassword=5
};

// 定义结构体
class Response
{
public:
    Response();
    ~Response();
    // 组织响应数据并发送
    void prepareMsg(Buffer* sendBuf, int socket);
    inline void setStatusCode(StatusCode code)
    {
        m_statusCode = code;
    }
    inline void setData(string data) {
        m_data = data;
    }
private:
    // 状态行: 状态码, 状态描述
    StatusCode m_statusCode;
    string m_data;
    // 定义状态码和描述的对应关系
    const map<int, string> m_info = {
        {1, "OK"},
        {2, "Failure"},
        {3, "Illegal Operation"},
        {4,"Repeated data"},
        {5,"Wrong password"}
    };
};

