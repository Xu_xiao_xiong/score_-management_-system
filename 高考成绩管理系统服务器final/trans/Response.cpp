#include "Response.h"
#include"Log.h"

Response::Response()
{
    m_statusCode = StatusCode::Unknown;
    m_data = "";
}

Response::~Response()
{

}




void Response::prepareMsg(Buffer* sendBuf, int socket)
{
   
    Value response;
    response["statusCode"] = (int)m_statusCode;
    response["info"] = m_info.at(int(m_statusCode));
    response["data"] = m_data.c_str();
    // 回复的数据
    FastWriter writer;
    sendBuf->appendString(writer.write(response));
Debug("发送的数据:%s",sendBuf->data());
    sendBuf->sendData(socket);
}
