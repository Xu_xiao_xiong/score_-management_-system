#pragma once
#include "Buffer.h"
#include "Response.h"
#include"json/json.h"
#include"MySqlConn.h"
using namespace std;
using namespace Json;

// 定义请求结构体

class Request
{
public:
    Request();
    ~Request();
    // 重置
    void reset();
    // 解析http请求协议
    bool parseRequest(Buffer* readBuf, Response* response, Buffer* sendBuf, int socket,MySqlConn *mysql);
    // 处理http请求协议
    bool processRequest(Response* response,MySqlConn*mysql);

    inline void setMethod(string method)
    {
        m_method = method;
    }
    inline void setChinese(int chinese)
    {
        m_chinese=chinese;
    }
    inline void setEnglish(int english)
    {
        m_english =english ;
    } 
    inline void setUni(int uni)
    {
        m_uni = uni;
    } 
    inline void setMath(int math)
    {
        m_math= math;
    }
  




private:
    string m_method;
    string m_id;
    string m_name;
    string m_job;
    int m_chinese,m_math,m_english,m_uni;
    string m_passwd;
    string m_account;

    bool isAdmi(MySqlConn* mysql);
    bool isUser(MySqlConn* mysql);
    bool isRepeat(MySqlConn*mysql);
    bool isRepeate(MySqlConn*mysql);
    void resort(MySqlConn* mysql);
};

