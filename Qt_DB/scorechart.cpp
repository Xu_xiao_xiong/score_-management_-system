#include "scorechart.h"
#include "ui_scorechart.h"
//从数据库中读取学生成绩

 bool readStudentsToVector(QVector<Student> &students)//等待服务端补充接口
{

}

scoreChart::scoreChart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::scoreChart)
{
    ui->setupUi(this);
    this->setWindowTitle("成绩分析");
}

bool dataProcessing(QVector<unsigned> &chinese,unsigned *chinese_arr,unsigned pace)//这个函数面对异常处理还需优化
{
    for(int i=0;i<chinese.size();++i)
    {
        unsigned section=chinese[i]/pace;
        if(chinese[i]==(pace*10)) section--;
        switch (section)
        {
        case 0:
        {
        chinese_arr[0]++;
        break;
        }
        case 1:
        {
        chinese_arr[1]++;
        break;
        }
        case 2:
        {
        chinese_arr[2]++;
        break;
        }
        case 3:
        {
        chinese_arr[3]++;
        break;
        }
        case 4:
        {
        chinese_arr[4]++;
        break;
        }
        case 5:
        {
        chinese_arr[5]++;
        break;
        }
        case 6:
        {
        chinese_arr[6]++;
        break;
        }
        case 7:
        {
        chinese_arr[7]++;
        break;
        }
        case 8:
        {
        chinese_arr[8]++;
        break;
        }
        case 9:
        {
        chinese_arr[9]++;
         break;
        }
        default:
           return false;
        }

    }
    return true;
}

void scoreChart::getServerData()
{
    Student stu;
    TCPClient tcp;
    QVector<QString> ID;
    tcp.getall2(ID);
    int num=ID.size();
    for(int i=0;i<num;i++)
    {
        TCPClient tcp;
        tcp.search(ID[i],stu);
        chinese.push_back(stu.Chinese);
        math.push_back(stu.Math);
        english.push_back(stu.English);
        comprehension.push_back(stu.Comprehension);
     }
}

void scoreChart::paintEvent(QPaintEvent *event)
{



    //从数据库里读取数据存放至QVector容器里
  /*
    QSqlDatabase db = QSqlDatabase::database("myDatabase");
       QSqlQuery query(db);
       query.prepare("SELECT stu_number, stu_name, Chinese_score,"
                     " Math_score, English_score, Comprehension_score, total_score FROM stu_info");


         //数据处理阶段
         //codes here...
         if (query.exec()) {
         while (query.next()) {
             // 创建 Student 对象并填充数据

            unsigned stu_chinese = query.value(2).toUInt();
            unsigned stu_math = query.value(3).toUInt();
            unsigned stu_english = query.value(4).toUInt();
            unsigned stu_comprehension = query.value(5).toUInt();
             //stu.total = query.value(6).toUInt();

             //将数据放入数组
            chinese.push_back(stu_chinese);
            math.push_back(stu_math);
            english.push_back(stu_english);
            comprehension.push_back(stu_comprehension);
         }
         }
         */
         unsigned chinese_arr[10]={0,0,0,0,0,0,0,0,0,0};
         unsigned math_arr[10]={0,0,0,0,0,0,0,0,0,0};
         unsigned english_arr[10]={0,0,0,0,0,0,0,0,0,0};
         unsigned comprehension_arr[10]={0,0,0,0,0,0,0,0,0,0};//四个存储每个区间数量的数组


         QPainter painter(this);//定义画笔
         painter.setRenderHint(QPainter::Antialiasing);
     /*绘制坐标轴-------------------------------------*/
             // 定义坐标轴的起点和长度
             int startX = 400;
             int startY = height() -100;
             int axisLength = width() - 600;
             int numTicks = 10;  // 刻度数量
             double tickSpacing = static_cast<double>(axisLength) / numTicks;//每一个刻度的长度
             int tickValue = 0;//x轴的刻度的值
             int full_mark=0;//学科的满分
             int tick_y=10;//y轴百分比

             switch (currentSubject)
             {
                 case None:
                     break;
                 case Chinese:
                 {
                  bool is_OK=dataProcessing(chinese,chinese_arr,15);
                  int stu_num=chinese.size();//学生总数
                  full_mark=150;
                  if(is_OK){

                 // 绘制坐标轴线
                 painter.drawLine(startX, startY, startX + axisLength+20, startY);  // X 轴
                 painter.drawLine(startX, startY, startX, 50);                   // Y 轴

                 // 绘制 X 轴箭头
                 painter.drawLine(startX + axisLength+20, startY,
                                  startX + axisLength +10, startY + 5);
                 painter.drawLine(startX + axisLength+20, startY,
                                  startX + axisLength + 10, startY - 5);

                 // 绘制 Y 轴箭头
                 painter.drawLine(startX, 50, startX - 5, 60);
                 painter.drawLine(startX, 50, startX + 5, 60);

                 // 标记坐标轴刻度
                 int numTicks = 10;  // 刻度数量
                 double tickSpacing = static_cast<double>(axisLength) / numTicks;
                 int tickValue = 0;

                 for (int i = 0; i <= numTicks; ++i) {
                     int x = startX + static_cast<int>(i * tickSpacing);
                     int y = startY;

                     painter.drawLine(x, y, x, y - 5);  // 绘制刻度线

                     // 绘制刻度值
                      QString label = QString::number(tickValue); // 格式化刻度值
                     QRect rect = QRect(x - 25, y + 15, 50, 20+10);  // 文本框位置
                     painter.drawText(rect, Qt::AlignHCenter, label);

                     tickValue += full_mark/10;  // 增加刻度值
                 }
                 //绘制y轴坐标
                 int tick_y=10;//y轴百分比
                 for(int i=0;i<numTicks;++i)
                 {
                     int x=startX;
                     int y=startY-static_cast<int>(i*tickSpacing);
                     painter.drawLine(x,y,x+5,y);
                     if(i<numTicks-1)
                     {
                         QString label=QString::number(tick_y)+"%";
                         QRect rect = QRect(x - 45, y -75, 50, 20+10);
                         painter.drawText(rect, Qt::AlignHCenter, label);
                         tick_y += 10;
                     }
                 }

                 // 绘制坐标轴标签
                 painter.drawText(QRect(startX + axisLength , startY + 10-10-10,90, 20+10+10), Qt::AlignHCenter, "成绩");
                 painter.drawText(QRect(startX , 30+10, 70, 50), Qt::AlignHCenter, "百分比");
                 //以上代码是绘制坐标轴的代码
                 /*以下绘制矩形长条*/
                     QColor color(255,0,0);
                     painter.setBrush(color);
                     for(int i=0;i<numTicks;i++)
                     {
                         int x = startX + static_cast<int>(i * tickSpacing);
                         int y = startY;
                         //于此处写上处理好的数据，绘制到界面上。
                         float unit_height=static_cast<float>(chinese_arr[i])/static_cast<float>(stu_num);
                         painter.drawRect(x,y-unit_height*axisLength,tickSpacing,unit_height*axisLength);
                         QFont font("宋体",7);
                         painter.setFont(font);
                         QString percent=QString::number(unit_height,'f',2)+"%";//只保留两位小数
                         QRect frame=QRect(x,y-unit_height*axisLength-40,tickSpacing-10,30);
                         painter.drawText(frame,  Qt::AlignHCenter,percent);
                     }
                  }
                  else
                  {
                     //异常提示
                  }
                 break;
                 }
                 /*----------------------------------------------------------------------*/
                 case Math:
                 {
                 full_mark=150;
                 int stu_num=math.size();
                 bool is_OK=dataProcessing(math,math_arr,15);
                 if(is_OK){
                 // 绘制坐标轴线
                 painter.drawLine(startX, startY, startX + axisLength+20, startY);  // X 轴
                 painter.drawLine(startX, startY, startX, 50);                   // Y 轴

                 // 绘制 X 轴箭头
                 painter.drawLine(startX + axisLength+20, startY,
                                  startX + axisLength +10, startY + 5);
                 painter.drawLine(startX + axisLength+20, startY,
                                  startX + axisLength + 10, startY - 5);

                 // 绘制 Y 轴箭头
                 painter.drawLine(startX, 50, startX - 5, 60);
                 painter.drawLine(startX, 50, startX + 5, 60);

                 // 标记坐标轴刻度
                 int numTicks = 10;  // 刻度数量
                 double tickSpacing = static_cast<double>(axisLength) / numTicks;
                 int tickValue = 0;

                 for (int i = 0; i <= numTicks; ++i) {
                     int x = startX + static_cast<int>(i * tickSpacing);
                     int y = startY;

                     painter.drawLine(x, y, x, y - 5);  // 绘制刻度线

                     // 绘制刻度值
                      QString label = QString::number(tickValue); // 格式化刻度值
                     QRect rect = QRect(x - 25, y + 15, 50, 20+10);  // 文本框位置
                     painter.drawText(rect, Qt::AlignHCenter, label);

                     tickValue += full_mark/10;  // 增加刻度值
                 }
                 //绘制y轴坐标
                 int tick_y=10;//y轴百分比
                 for(int i=0;i<numTicks;++i)
                 {
                     int x=startX;
                     int y=startY-static_cast<int>(i*tickSpacing);
                     painter.drawLine(x,y,x+5,y);
                     if(i<numTicks-1)
                     {
                         QString label=QString::number(tick_y)+"%";
                         QRect rect = QRect(x - 45, y -75, 50, 20+10);
                         painter.drawText(rect, Qt::AlignHCenter, label);
                         tick_y += 10;
                     }
                 }

                 // 绘制坐标轴标签
                 painter.drawText(QRect(startX + axisLength , startY + 10-10-10,90, 20+10+10), Qt::AlignHCenter, "成绩");
                 painter.drawText(QRect(startX , 30+10, 70, 50), Qt::AlignHCenter, "百分比");
                 QColor color_2(0,0,255);
                 painter.setBrush(color_2);
                 for(int i=0;i<numTicks;i++)
                     {
                     int x = startX + static_cast<int>(i * tickSpacing);
                     int y = startY;
                     //于此处写上处理好的数据，绘制到界面上。
                     float unit_height=static_cast<float>(math_arr[i])/static_cast<float>(stu_num);
                     painter.drawRect(x,y-unit_height*axisLength,tickSpacing,unit_height*axisLength);
                     QFont font("宋体",7);
                     painter.setFont(font);
                     QString percent=QString::number(unit_height,'f',2)+"%";//只保留两位小数
                     QRect frame=QRect(x,y-unit_height*axisLength-40,tickSpacing-10,30);
                     painter.drawText(frame,  Qt::AlignHCenter,percent);
                     }
                 break;
                 }
                 else
                 {
                     //异常提示
                 break;
                 }
                 }
             case English:
                 {
                     int stu_num=english.size();
                     bool is_ok=dataProcessing(english,english_arr,15);
                     full_mark=150;
                     // 绘制坐标轴线
                     if(is_ok){
                     painter.drawLine(startX, startY, startX + axisLength+20, startY);  // X 轴
                     painter.drawLine(startX, startY, startX, 50);                   // Y 轴

                     // 绘制 X 轴箭头
                     painter.drawLine(startX + axisLength+20, startY,
                                      startX + axisLength +10, startY + 5);
                     painter.drawLine(startX + axisLength+20, startY,
                                      startX + axisLength + 10, startY - 5);

                     // 绘制 Y 轴箭头
                     painter.drawLine(startX, 50, startX - 5, 60);
                     painter.drawLine(startX, 50, startX + 5, 60);

                     // 标记坐标轴刻度
                     int numTicks = 10;  // 刻度数量
                     double tickSpacing = static_cast<double>(axisLength) / numTicks;
                     int tickValue = 0;

                     for (int i = 0; i <= numTicks; ++i) {
                         int x = startX + static_cast<int>(i * tickSpacing);
                         int y = startY;

                         painter.drawLine(x, y, x, y - 5);  // 绘制刻度线

                         // 绘制刻度值
                          QString label = QString::number(tickValue); // 格式化刻度值
                         QRect rect = QRect(x - 25, y + 15, 50, 20+10);  // 文本框位置
                         painter.drawText(rect, Qt::AlignHCenter, label);

                         tickValue += full_mark/10;  // 增加刻度值
                     }
                     //绘制y轴坐标
                     int tick_y=10;//y轴百分比
                     for(int i=0;i<numTicks;++i)
                     {
                         int x=startX;
                         int y=startY-static_cast<int>(i*tickSpacing);
                         painter.drawLine(x,y,x+5,y);
                         if(i<numTicks-1)
                         {
                             QString label=QString::number(tick_y)+"%";
                             QRect rect = QRect(x - 45, y -75, 50, 20+10);
                             painter.drawText(rect, Qt::AlignHCenter, label);
                             tick_y += 10;
                         }
                     }

                     // 绘制坐标轴标签
                     painter.drawText(QRect(startX + axisLength , startY + 10-10-10,90, 20+10+10), Qt::AlignHCenter, "成绩");
                     painter.drawText(QRect(startX , 30+10, 70, 50), Qt::AlignHCenter, "百分比");
                     /*以下绘制矩形长条*/
                         QColor color(0,255,0);
                         painter.setBrush(color);
                         for(int i=0;i<numTicks;i++)
                         {
                             int x = startX + static_cast<int>(i * tickSpacing);
                             int y = startY;
                             //于此处写上处理好的数据，绘制到界面上。
                             float unit_height=static_cast<float>(english_arr[i])/static_cast<float>(stu_num);
                             painter.drawRect(x,y-unit_height*axisLength,tickSpacing,unit_height*axisLength);
                             QFont font("宋体",7);
                             painter.setFont(font);
                             QString percent=QString::number(unit_height,'f',2)+"%";//只保留两位小数
                             QRect frame=QRect(x,y-unit_height*axisLength-40,tickSpacing-10,30);
                             painter.drawText(frame,  Qt::AlignHCenter,percent);
                         }
                         break;
                    }
                 else
                 {
                         //异常提示
                     break;
                 }
                 }
             case Comprehension:
             {
                 full_mark=300;
                 int stu_num=comprehension.size();
                 bool is_OK=dataProcessing(comprehension,comprehension_arr,30);
                 if(is_OK)
                 {
                     painter.drawLine(startX, startY, startX + axisLength+20, startY);  // X 轴
                     painter.drawLine(startX, startY, startX, 50);                   // Y 轴

                     // 绘制 X 轴箭头
                     painter.drawLine(startX + axisLength+20, startY,
                                      startX + axisLength +10, startY + 5);
                     painter.drawLine(startX + axisLength+20, startY,
                                      startX + axisLength + 10, startY - 5);

                     // 绘制 Y 轴箭头
                     painter.drawLine(startX, 50, startX - 5, 60);
                     painter.drawLine(startX, 50, startX + 5, 60);

                     // 标记坐标轴刻度
                     int numTicks = 10;  // 刻度数量
                     double tickSpacing = static_cast<double>(axisLength) / numTicks;
                     int tickValue = 0;

                     for (int i = 0; i <= numTicks; ++i) {
                         int x = startX + static_cast<int>(i * tickSpacing);
                         int y = startY;

                         painter.drawLine(x, y, x, y - 5);  // 绘制刻度线

                         // 绘制刻度值
                          QString label = QString::number(tickValue); // 格式化刻度值
                         QRect rect = QRect(x - 25, y + 15, 50, 20+10);  // 文本框位置
                         painter.drawText(rect, Qt::AlignHCenter, label);

                         tickValue += full_mark/10;  // 增加刻度值
                     }
                     //绘制y轴坐标
                     int tick_y=10;//y轴百分比
                     for(int i=0;i<numTicks;++i)
                     {
                         int x=startX;
                         int y=startY-static_cast<int>(i*tickSpacing);
                         painter.drawLine(x,y,x+5,y);
                         if(i<numTicks-1)
                         {
                             QString label=QString::number(tick_y)+"%";
                             QRect rect = QRect(x - 45, y -75, 50, 20+10);
                             painter.drawText(rect, Qt::AlignHCenter, label);
                             tick_y += 10;
                         }
                     }

                     // 绘制坐标轴标签
                     painter.drawText(QRect(startX + axisLength , startY + 10-10-10,90, 20+10+10), Qt::AlignHCenter, "成绩");
                     painter.drawText(QRect(startX , 30+10, 70, 50), Qt::AlignHCenter, "百分比");
                     /*以下绘制矩形长条*/
                         QColor color(255,165,0);//理综为橙色
                         painter.setBrush(color);
                         for(int i=0;i<numTicks;i++)
                         {
                             int x = startX + static_cast<int>(i * tickSpacing);
                             int y = startY;
                             //于此处写上处理好的数据，绘制到界面上。
                             float unit_height=static_cast<float>(comprehension_arr[i])/static_cast<float>(stu_num);
                             painter.drawRect(x,y-unit_height*axisLength,tickSpacing,unit_height*axisLength);
                             QFont font("宋体",7);
                             painter.setFont(font);
                             QString percent=QString::number(unit_height,'f',2)+"%";//只保留两位小数
                             QRect frame=QRect(x,y-unit_height*axisLength-40,tickSpacing-10,30);
                             painter.drawText(frame,  Qt::AlignHCenter,percent);
                         }
                         break;
                 }
                 else
                 {
                    //异常提示
                 break;
                 }
             }
             }

}

scoreChart::~scoreChart()
{
    delete ui;
}

void scoreChart::on_backButton_clicked()
{
    emit this->backMenu();
}

void scoreChart::on_chineseButton_clicked()
{
    currentSubject=Chinese;
    getServerData();
    update();
}

void scoreChart::on_mathButton_clicked()
{
    currentSubject=Math;
    getServerData();
    update();
}

void scoreChart::on_englishButton_clicked()
{
    currentSubject=English;
    getServerData();
    update();
}

void scoreChart::on_comprehensionButton_clicked()
{
    currentSubject=Comprehension;
    getServerData();
    update();
}
