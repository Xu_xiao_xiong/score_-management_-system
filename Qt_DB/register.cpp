#include "register.h"
#include "ui_register.h"
#include <QPushButton>


Register::Register(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Register)
{
    ui->setupUi(this);
    //将两个密码框设置为加密模式
    ui->password->setEchoMode(QLineEdit::Password);
    ui->password_2->setEchoMode(QLineEdit::Password);
    //发送回到登陆界面的信号
    connect(ui->retButton,&QPushButton::clicked,[=](){
        //发送信号
        emit this->backLogin();
    });
   //更改窗口的名字
    this->setWindowTitle("注册");

}


Register::~Register()
{
    delete ui;
}


void Register::on_regisButton_clicked()//注册按钮的槽函数
{
    /*以下为获得文本框内信息*/
    QString user_name=ui->name->text();//用户姓名
    QString user_id=ui->id->text();//用户ID
    QString user_password=ui->password->text();//用户密码
    QString user_check_word=ui->password_2->text();//用户确认密码
    QString user_job=ui->jobBox->currentText();//用户职位
    //判断输入不能为空
    if(user_name.isEmpty()||user_id.isEmpty()||user_password.isEmpty())
    {
        QMessageBox::warning(this,"提示","输入不能为空");
        return;
    }
    //关于密码不一致的判断
    if(user_password!=user_check_word)
    {
         QMessageBox::warning(this,"提示","两次密码输入不一致");
         return;
    }
    TCPClient tcp;

    if(tcp.myregister(user_id,user_job,user_name,user_password))
    {
        QMessageBox::information(this,"注册提示","注册成功！");
    }
    else
    {
        QMessageBox::warning(this,"注册提示","注册失败！");
        return;
    }
    /*
    QFile fw(filename);
    if (!fw.open(QIODevice::Append | QIODevice::Text))
    {
        QMessageBox::warning(this,"警告","打开文件失败");
        return; // 可以处理打开失败的情况
    }
    QTextStream out(&fw); // 创建文件输入流对象
    out.setCodec("UTF-8");//设置编码
    out<<user_name<<" "<<user_id<<" "<<user_password<<" "<<user_job<<"\n";//向文件写入信息
    QMessageBox::information(this,"注册","注册成功！");
      emit this->backLogin_2();//注册成功后，发送消息回到登陆界面
    fw.close();//关闭文件
    */
}

void Register::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawPixmap(0,0,width(),height(),QPixmap(BACKGROUND_PATH));
}
