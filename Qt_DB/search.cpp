#include "search.h"
#include "ui_search.h"

Search::Search(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Search)
{
    ui->setupUi(this);
    this->setWindowTitle("查询");
}

Search::~Search()
{
    delete ui;
}

void Search::on_backButton_clicked()
{
    emit this->backMenu();
}

void Search::on_searchButton_clicked()
{
    ui->name->setText("姓名：");
    ui->id->setText("学号：");
    ui->chinese->setText("语文：");
    ui->math->setText("数学：");
    ui->english->setText("英语：");
    ui->comprehension->setText("综合：");
    ui->total->setText("总分：");

    QString num=ui->stu_num->text();

    TCPClient tcp;
    Student stu;
    if(tcp.search(num,stu))
    {
        //判断搜索成功后，把数据存在一个学生类里，然后打印在图形化界面里

     QString name_str=ui->name->text()+stu.name;
     QString id_str=ui->id->text()+stu.ID;
     QString chinese_str=ui->chinese->text()+QString::number(stu.Chinese);
     QString math_str=ui->math->text()+QString::number(stu.Math);
     QString english_str=ui->english->text()+QString::number(stu.English);
     QString comprehension_str=ui->comprehension->text()+QString::number(stu.Comprehension);
     QString total_str=ui->total->text()+QString::number(stu.total);
     //图形化界面目前使用的是最简单的label控件，可以用，就是难看了点，后期再优化
     ui->name->setText(name_str);
     ui->id->setText(id_str);
     ui->chinese->setText(chinese_str);
     ui->math->setText(math_str);
     ui->english->setText(english_str);
     ui->comprehension->setText(comprehension_str);
     ui->total->setText(total_str);
    }
    else
    {
        QMessageBox::warning(this,"查找提示","查询失败");
    }

}


void Search::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawPixmap(0,0,width(),height(),QPixmap(BACKGROUND_PATH));
}
