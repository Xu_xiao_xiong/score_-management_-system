#include "loggingdata.h"
#include "ui_loggingdata.h"

LoggingData::LoggingData(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoggingData)
{
    ui->setupUi(this);
    //改变界面名称
    this->setWindowTitle("录入学生成绩");
}

LoggingData::~LoggingData()
{
    delete ui;
    //db.close();
    delete m_xlsx;
}

void LoggingData::on_backButton_clicked()//返回主菜单的槽函数
{
    emit this->backMenu();
}

void LoggingData::on_singleLoggingButton_clicked()
{
    bool all_digit=true;//判断输入的成绩是否全部为数字
    QString name=ui->stu_name->text();
    QString id=ui->stu_id->text();
    size_t chi_length=ui->stu_chinese->text().length();
    size_t math_length=ui->stu_math->text().length();
    size_t eng_length=ui->stu_english->text().length();
    size_t com_length=ui->stu_comprehension->text().length();

    if(chi_length==0||math_length==0||eng_length==0||com_length==0||name.size()==0||id.size()==0)
    {
        QMessageBox::warning(this,"输入提示","输入不得为空！");
        return;
    }//异常处理,输入空

    for(size_t i=0;i<chi_length;i++)
    {
       if(! ui->stu_chinese->text().at(i).isDigit())
       {
           all_digit=false;
           break;
       }
    }
    if(all_digit)
    {
        for(size_t i=0;i<math_length;i++)
        {

            if(!ui->stu_math->text().at(i).isDigit())
            {
            all_digit=false;
            break;
            }
        }
    }
    if(all_digit)
    {
    for(size_t i=0;i<eng_length;i++)
    {

        if(!ui->stu_english->text().at(i).isDigit())
        {
            all_digit=false;
            break;
        }
    }
    }
    if(all_digit)
    {
        for(size_t i=0;i<com_length;i++)
        {

            if(!ui->stu_comprehension->text().at(i).isDigit())
            {
            all_digit=false;
            break;
            }
        }
    }
    if(!all_digit)
    {
        QMessageBox::warning(this,"输入提示","成绩输入不得为非数字！");
        return;
    }//异常处理1：输入分数非数字
    unsigned chinese=ui->stu_chinese->text().toUInt();
    unsigned math=ui->stu_math->text().toUInt();
    unsigned english=ui->stu_english->text().toUInt();
    unsigned comprehension=ui->stu_comprehension->text().toUInt();
    unsigned int total=chinese+math+english+comprehension;

    if(chinese>150||math>150||english>150||comprehension>300)
    {
        QMessageBox::warning(this,"输入提示","分数输入有误！");
        return;
    }
    //异常处理2：输入分数超标
    Student stu;
    stu.ID=id;
    stu.name=name;
    stu.Chinese=chinese;
    stu.Math=math;
    stu.English=english;
    stu.Comprehension=comprehension;
    stu.total=chinese+math+english+comprehension;
    TCPClient tcp;
    if(tcp.addstudent(stu))
    {
        QMessageBox::information(this,"录入提示","录入成功！");
    }
 else
    {
        QMessageBox::warning(this,"录入提示","录入失败！");
    }

}

void LoggingData::on_bulkImportButton_clicked()
{
    QString filePath = ui->file_path->text(); // 获取文件路径
    // 读取Excel文件
    m_xlsx = new QXlsx::Document(filePath, this);
    if (m_xlsx) {
        int row = m_xlsx->dimension().rowCount();
        Student stu;
        int i=2;
        for(i;i<=row;i++)
        {
            stu.ID=m_xlsx->read(i,1).toString();
            stu.name=m_xlsx->read(i,2).toString();
            stu.Chinese=m_xlsx->read(i,3).toUInt();
            stu.Math=m_xlsx->read(i,4).toUInt();
            stu.English=m_xlsx->read(i,5).toUInt();
            stu.Comprehension=m_xlsx->read(i,6).toUInt();
            stu.total=m_xlsx->read(i,7).toUInt();
            TCPClient tcp;
            tcp.addstudent(stu);
           // QThread::sleep(1);

        }
        if(i==row+1)
        {
            QMessageBox::information(this,"导入提示","数据全部导入成功！");
        }
        else
        {
            QMessageBox::warning(this,"导入提示","数据未全部导入！");
        }

    }
    else
    {
        qDebug()<<"failed to open the file";
    }

}

void LoggingData::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawPixmap(0,0,width(),height(),QPixmap(BACKGROUND_PATH));
}
