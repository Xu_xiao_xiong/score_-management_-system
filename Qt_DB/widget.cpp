#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //新建后两个页面的对象
    //this->rl=new ScoreRelation;//初始化线性关系界面
    this->page=new Widget2;
    this->reg =new Register;
    //将密码框设置为密码模式
      ui->password->setEchoMode(QLineEdit::Password);

    //这是一个lambda表达式，用于关联注册界面和登录界面界面切换的函数
    connect(this->reg,&Register::backLogin,[=](){
        this->reg->hide();
        this->show();
    });

    connect(this->reg,&Register::backLogin_2,[=](){
        this->reg->hide();
        this->show();
    });

    //关联菜单界面和用户登陆界面的函数
    connect(this->page,&Widget2::backLogin,[=](){
        this->page->hide();
        this->show();
    });
    //更改窗口的名字
    this->setWindowTitle("登录");
}

Widget::~Widget()
{
    delete ui;
}




void Widget::on_registerButton_clicked()
{
    this->hide();
    this->reg->show();
}


void Widget::on_logInButton_clicked()
{
    //获取文本框内字符字符串的输入
    QString user_name=ui->username->text();
    QString user_pass=ui->password->text();
    TCPClient::username=user_name;
    TCPClient::userpasswd=user_pass;
    TCPClient tcp;
    if(tcp.mylogin())
    {
        QMessageBox::information(this,"登录"," 登陆成功！");
        this->hide();
        page->show();
    }
    else
    {
        QMessageBox::warning(this,"登录"," 登陆失败！");
    }

    /*
    //打开文件
    QFile fr(filename);
     if (!fr.open(QIODevice::ReadOnly | QIODevice::Text))
     {
         QMessageBox::warning(this,"提示","打开文件失败");
         return;
     }
     QTextStream in(&fr);
      in.setCodec("UTF-8");//设置编码
     while(!in.atEnd())
     {
      QString line=in.readLine();
      QStringList line_array=line.split(" ", Qt::SkipEmptyParts);
    //判断账号密码
      if(user_name!=line_array[1]&&user_name!=line_array[0])
      {

          continue;
      }
      if(user_pass!=line_array[2])
      {

          continue;
      }
      ok=true;
      break;
     }
     fr.close();//关闭文件
     if(ok)
     {
        //QMessageBox::warning(this,"登录"," 登陆成功！");
         this->hide();
         page->show();
     }
     else
     {
         QMessageBox::warning(this,"登录"," 登陆失败！");
     }
     */

}


void Widget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawPixmap(0,0,width(),height(),QPixmap(BACKGROUND_PATH));
}
