#include "tableview.h"
#include "ui_tableview.h"

TableView::TableView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TableView)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(8);
    QStringList header;
    header<<"学号"<<"姓名"<<"语文"<<"数学"<<"英语"<<"综合"<<"总分"<<"排名";
    ui->tableWidget->setHorizontalHeaderLabels(header);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(7,QHeaderView::Fixed);
    //ui->tableWidget->setColumnWidth(0,200);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(7,QHeaderView::Stretch);


    ui->tableWidget->horizontalHeader()->setFont(QFont("黑体",16));
    ui->tableWidget->setAlternatingRowColors(true);

}

TableView::~TableView()
{
    delete ui;
}

void TableView::insertTableItems(QVector<Student> stus, int num)
{
    ui->tableWidget->setRowCount(num); // 设置表格的行数为学生数量

    for (int i = 0; i < num; ++i) {
        // 创建表格项并设置值
        QTableWidgetItem *itemId = new QTableWidgetItem(stus[i].ID);
        QTableWidgetItem *itemName = new QTableWidgetItem(stus[i].name);
        QTableWidgetItem *itemChinese = new QTableWidgetItem(QString::number(stus[i].Chinese));
        QTableWidgetItem *itemMath = new QTableWidgetItem(QString::number(stus[i].Math));
        QTableWidgetItem *itemEnglish = new QTableWidgetItem(QString::number(stus[i].English));
        QTableWidgetItem *itemComprehension = new QTableWidgetItem(QString::number(stus[i].Comprehension));
        QTableWidgetItem *itemTotal = new QTableWidgetItem(QString::number(stus[i].total));
        QTableWidgetItem *itemRank = new QTableWidgetItem(QString::number(stus[i].rank));

        // 将表格项添加到表格中
        ui->tableWidget->setItem(i, 0, itemId);
        ui->tableWidget->setItem(i, 1, itemName);
        ui->tableWidget->setItem(i, 2, itemChinese);
        ui->tableWidget->setItem(i, 3, itemMath);
        ui->tableWidget->setItem(i, 4, itemEnglish);
        ui->tableWidget->setItem(i, 5, itemComprehension);
        ui->tableWidget->setItem(i, 6, itemTotal);
        ui->tableWidget->setItem(i, 7, itemRank);
    }
}


QVector<Student> TableView::getStus()
{
    QVector<Student>stus;
    QVector<QString>ID;
    Student stu;
    TCPClient tcp;
    tcp.getall2(ID);
    for(int i=0;i<ID.size();i++)
    {
        TCPClient tcp1;
        tcp1.search(ID[i],stu);
        stus.push_back(stu);
    }
    return stus;
}

// 比较函数，用于升序排序
bool TableView::compareByTotalAscending(const Student &a, const Student &b) {
    return a.total < b.total;
}

// 比较函数，用于降序排序
bool TableView::compareByTotalDescending(const Student &a, const Student &b) {
    return a.total > b.total;
}
void TableView::on_uporeder_clicked()
{
    QVector<Student> stus = getStus();
        // 使用 std::bind 将非静态成员函数与 this 绑定
    std::sort(stus.begin(), stus.end(), std::bind(&TableView::compareByTotalDescending, this, std::placeholders::_1, std::placeholders::_2));
        // 更新视图
       insertTableItems(stus,stus.size());

}

void TableView::on_downorder_clicked()
{
    QVector<Student> stus = getStus();
        // 使用 std::bind 将非静态成员函数与 this 绑定
    std::sort(stus.begin(), stus.end(), std::bind(&TableView::compareByTotalAscending, this, std::placeholders::_1, std::placeholders::_2));
        // 更新视图
    insertTableItems(stus,stus.size());
}


void TableView::on_backButton_clicked()
{
    emit this->backMenu();
}
