#include "scorerelation.h"
#include "ui_scorerelation.h"

ScoreRelation::ScoreRelation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScoreRelation)
{
    ui->setupUi(this);
    this->setWindowTitle("成绩线性关系");
    //
}

ScoreRelation::~ScoreRelation()
{
    delete ui;
}

void ScoreRelation::on_backButton_clicked()
{
    emit this->backMenu();
}

bool ScoreRelation::getData(QVector<uint> &Chi_Mat_Eng_scores,QVector<uint> &Comprehension_scores)//等待服务端补充接口
{
    Student stu;
    TCPClient tcp;
    QVector<QString> ID;
    bool flag=tcp.getall2(ID);
    int num=ID.size();
    for(int i=0;i<num;i++)
    {
        TCPClient tcp;
        tcp.search(ID[i],stu);
        Chi_Mat_Eng_scores.push_back(stu.Chinese+stu.Math+stu.English);
        Comprehension_scores.push_back(stu.Comprehension);
    }
    if(flag)return true;
    else return false;
}

float LinearRegression(QVector<float>&x,QVector<float>&y)
{
    float  meanX=0.0,meanY=0.0;
    for(int i=0;i<x.size();++i)
    {
        meanX+=x[i];
        meanY+=y[i];
    }
    meanX/=x.size();
    meanY/=y.size();
    float numerator = 0.0, denominator = 0.0;
    for (int i = 0; i < x.size(); ++i) {
        numerator += (x[i] - meanX) * (y[i] - meanY);
        denominator += (x[i] - meanX) * (x[i] - meanX);
    }
    float slope = numerator / denominator;
    return slope;
}

float linearFunction(float slope,float intercept,float x)//一次函数，返回y（ax+b）
{
    return  slope*x+intercept;
}

void ScoreRelation::initChart()
{
    //创建QChart对象（图表数学模型）
    if(m_chart==nullptr)
    {
        m_chart = new QChart();
    }
    //创建折线对象
    if(m_line==nullptr)
    {
        m_line =new QLineSeries();
        m_line->setName("relation");//添加名称
        m_chart->addSeries(m_line);
    }
    //创建X轴对象
    if(m_axisX==nullptr)
    {

        m_axisX = new QValueAxis();
        m_axisX->setRange(0,450);//设置X轴长度
        m_axisX->setGridLineVisible(false);//格子线
        m_axisX->setTickCount(11);
      //  m_axisX->setMinorTickCount(2);
        m_axisX->setTitleText("语数英总分");
    }
    //创建Y轴对象
    if(m_axisY==nullptr)
    {

        m_axisY = new QValueAxis();
        m_axisY->setRange(0,300);//设置Y轴长度
        m_axisY->setGridLineVisible(false);//格子线
        m_axisY->setTickCount(11);
      //  m_axisY->setMinorTickCount(2);
        m_axisY->setTitleText("综合");
    }

    //将X轴和Y轴添加到数学模型QChart中
    m_chart->addAxis(m_axisX,Qt::AlignBottom);
    m_chart->addAxis(m_axisY,Qt::AlignLeft);

    //将折线附加到两个轴上
    m_line->attachAxis(m_axisX);
    m_line->attachAxis(m_axisY);

    //隐藏图例
    m_chart->legend()->hide();

    ui->graphicsView->setChart(m_chart);

    ui->graphicsView->setRenderHint(QPainter::Antialiasing);

    QVector<unsigned> x;
    QVector<unsigned> y;
    bool is_OK=getData(x,y);
    if(is_OK)
    {
    float  meanX=0.0,meanY=0.0;
    for(int i=0;i<x.size();++i)
    {
        meanX+=x[i];
        meanY+=y[i];
    }
    meanX/=x.size();
    meanY/=y.size();
    float numerator = 0.0, denominator = 0.0;
    for (int i = 0; i < x.size(); ++i) {
        numerator += (x[i] - meanX) * (y[i] - meanY);
        denominator += (x[i] - meanX) * (x[i] - meanX);
    }
    float slope = numerator / denominator;
    float b=meanY-slope*meanX;
    float start_y=linearFunction(slope,b,0);
    float end_y=linearFunction(slope,b,440);
    m_line->append(0,start_y);
    m_line->append(440,end_y);
    QString equationLabel="y = " + QString::number(slope,'f',3) + "x + " + QString::number(b,'f',3);
    //layout()->addWidget(equationLabel);
   ui->linearFunc->setText(equationLabel);
   ui->linearFunc->move(meanX+200,meanY+200);
   qDebug()<<start_y<<" "<<end_y;
    }
    else
    {
        qDebug()<<"数据读取失败!";
        return ;
    }


}

void ScoreRelation::on_showButton_clicked()
{
    initChart();
}
