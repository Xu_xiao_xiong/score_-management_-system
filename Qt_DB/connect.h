#ifndef CONNECT_H
#define CONNECT_H

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QMessageBox>
#include<student.h>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QString>
#include <QRegularExpression>
#include <QVector>

class TCPClient : public QObject
{

public:
    // 静态成员变量声明
       static QString username;
       static QString userpasswd;
    explicit TCPClient(QObject *parent = nullptr);
    ~TCPClient();
    bool addstudent(const Student &stu );
    bool mydelete(QString ID);
    bool myupdate(const Student&stu);
    bool search(QString ID,Student& stu);
    bool myregister(QString name,QString job,QString account,QString passwd);
    bool mylogin();
    int getstucount();
    bool getall(QVector<Student> &stus);
    bool getall2(QVector<QString> &stus_ID);
    QVector<QString> extractNumbers(const QString &str);
//public slots:
    void onConnected();
    bool onReadyRead();
    void onDisconnected();
    void onError(QAbstractSocket::SocketError socketError);

private:
    QTcpSocket *socket;

};

#endif // CONNECT_H


