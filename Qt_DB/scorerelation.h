#ifndef SCORERELATION_H
#define SCORERELATION_H

#include <QDialog>
#include <QSqlDatabase>
#include<QSqlQuery>
#include<QDebug>
#include<QVector>
#include<student.h>
#include<QMessageBox>
#include <QtCharts>
#include<connect.h>
namespace Ui {
class ScoreRelation;
}

class ScoreRelation : public QDialog
{
    Q_OBJECT

signals:
    void backMenu();//回到主菜单的信号

public:
    explicit ScoreRelation(QWidget *parent = nullptr);
    ~ScoreRelation();
    bool getData(QVector<uint> &Chi_Mat_Eng_scores,QVector<uint> &Comprehension_scores);

private slots:
    void on_backButton_clicked();


    void on_showButton_clicked();

private:
    void initChart();

private:
    Ui::ScoreRelation *ui;
    //QSqlDatabase db;
    QChart * m_chart = nullptr;//
        QValueAxis * m_axisX=nullptr;
        QValueAxis * m_axisY = nullptr;
        QLineSeries *m_line = nullptr;
};

#endif // SCORERELATION_H
