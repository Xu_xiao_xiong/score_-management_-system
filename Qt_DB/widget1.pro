QT       += core gui sql
QT       += charts
QT       += core network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11




# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    connect.cpp \
    loggingdata.cpp \
    main.cpp \
    modify.cpp \
    register.cpp \
    scorechart.cpp \
    scorerelation.cpp \
    search.cpp \
    student.cpp \
    tableview.cpp \
    widget.cpp \
    widget2.cpp

HEADERS += \
    connect.h \
    loggingdata.h \
    modify.h \
    picture.h \
    register.h \
    scorechart.h \
    scorerelation.h \
    search.h \
    student.h \
    tableview.h \
    widget.h \
    widget2.h

FORMS += \
    loggingdata.ui \
    modify.ui \
    register.ui \
    scorechart.ui \
    scorerelation.ui \
    search.ui \
    tableview.ui \
    widget.ui \
    widget2.ui



include(src/xlsx/qtxlsx.pri)
QT += core gui xml



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
