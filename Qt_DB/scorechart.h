#ifndef SCORECHART_H
#define SCORECHART_H

#include <QWidget>
#include <QPainter>
#include <QVector>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QString>
#include <vector>
#include <QDebug>
#include<student.h>
#include<QSqlError>
#include<connect.h>
namespace Ui {
class scoreChart;
}

class scoreChart : public QWidget
{
    Q_OBJECT

public:
    explicit scoreChart(QWidget *parent = nullptr);
    ~scoreChart();

signals:
    void backMenu();//回到主菜单的信号
private slots:
    void on_backButton_clicked();

    void on_chineseButton_clicked();

    void on_mathButton_clicked();

    void on_englishButton_clicked();

    void on_comprehensionButton_clicked();

private:
    Ui::scoreChart *ui;
    virtual void paintEvent(QPaintEvent *event);
    void getServerData();
    enum Subject{None,Chinese,Math,English,Comprehension};//自定义一个枚举类型，包含各科成绩
    Subject currentSubject=None;//当前展示的科目成绩
    QVector<unsigned>chinese;//假设存好了
    QVector<unsigned>math;
    QVector<unsigned>english;
    QVector<unsigned>comprehension;

};

#endif // SCORECHART_H
