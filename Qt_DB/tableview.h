#ifndef TABLEVIEW_H
#define TABLEVIEW_H

#include <QWidget>
#include <QSqlDatabase>
#include<QSqlQuery>
#include<QDebug>
#include<QVector>
#include<QMessageBox>
#include<QSqlTableModel>
#include<QStandardItemModel>
#include<connect.h>
#include<student.h>
namespace Ui {
class TableView;
}

class TableView : public QWidget
{
    Q_OBJECT

public:
    explicit TableView(QWidget *parent = nullptr);
    ~TableView();

private slots:
    void on_uporeder_clicked();

    void on_downorder_clicked();
    QVector<Student> getStus();
     bool compareByTotalAscending(const Student &a, const Student &b);
     bool compareByTotalDescending(const Student &a, const Student &b);
    void on_backButton_clicked();

    void insertTableItems(QVector<Student> stus,int num);

signals:
    void backMenu();

private:
    Ui::TableView *ui;
    //QSqlDatabase db;
    //QSqlTableModel*m;
    //int currentPage = 0;
    //int rowsPerPage = 10; // 每页显示的行数
    //void applyPagination();
    //int total_row;
};

#endif // TABLEVIEW_H
