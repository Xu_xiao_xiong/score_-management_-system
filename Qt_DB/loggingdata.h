#ifndef LOGGINGDATA_H
#define LOGGINGDATA_H

#include <QWidget>
#include <QMessageBox>
#include "student.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QFile>
#include <QtCore>
#include "xlsxdocument.h"
#include "xlsxworksheet.h"
#include<student.h>
#include<connect.h>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QPainter>
#include "picture.h"
namespace Ui {
class LoggingData;
}

class LoggingData : public QWidget
{
    Q_OBJECT

public:
    explicit LoggingData(QWidget *parent = nullptr);
    ~LoggingData();

signals:
    void backMenu();
private slots:
    void on_backButton_clicked();

    void on_singleLoggingButton_clicked();

    void on_bulkImportButton_clicked();

private:
    //QSqlDatabase db;
     void paintEvent(QPaintEvent *event);//绘制函数
    Ui::LoggingData *ui;
    QXlsx::Document* m_xlsx = nullptr;
};

#endif // LOGGINGDATA_H
