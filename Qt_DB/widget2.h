#ifndef WIDGET2_H
#define WIDGET2_H

#include <QWidget>

#include "scorechart.h"
#include "loggingdata.h"
#include "search.h"
#include "modify.h"
#include "scorerelation.h"
#include"tableview.h"
#include<connect.h>
#include "picture.h"
namespace Ui {
class Widget2;
}

class Widget2 : public QWidget
{
    Q_OBJECT

public:
    explicit Widget2(QWidget *parent = nullptr);
    ~Widget2();

signals:
    void backLogin();//返回登录界面
private slots:
    void on_backButton_clicked();

    void on_scoreButton_clicked();

    void on_loggingDataButton_clicked();

    void on_searchButton_clicked();

    void on_modifyButton_clicked();

    void on_relationButton_clicked();

    void on_show_all_scores_clicked();

private:
    virtual void paintEvent(QPaintEvent *event);
private:
    Ui::Widget2 *ui;
    scoreChart *ct;//分数直方图界面的指针
    LoggingData *ld;//录入学生成绩界面的指针
    Search * sr;//查询界面的界面指针
    Modify *md;//修改界面的指针
    ScoreRelation *rl;//线性关系分析的界面
    TableView *tb;//全部成绩展示的界面
    TCPClient *tcp;//服务器指针

};

#endif // WIDGET2_H
