#include "modify.h"
#include "ui_modify.h"

Modify::Modify(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Modify)
{
    ui->setupUi(this);
    this->setWindowTitle("修改");
}

Modify::~Modify()
{
    delete ui;
}

void Modify::on_backButton_clicked()
{
    emit this->backMenu();
}

void Modify::on_modifyButton_clicked()
{
    bool all_digit=true;//判断输入的成绩是否全部为数字
    QString name=ui->stu_name->text();
    QString id=ui->stu_id->text();
    size_t chi_length=ui->stu_chinese->text().length();
    size_t math_length=ui->stu_math->text().length();
    size_t eng_length=ui->stu_english->text().length();
    size_t com_length=ui->stu_comprehension->text().length();
    for(size_t i=0;i<chi_length;i++)
    {
       if(! ui->stu_chinese->text().at(i).isDigit())
       {
           all_digit=false;
           break;
       }
    }
    if(all_digit)
    {
        for(size_t i=0;i<math_length;i++)
        {

            if(!ui->stu_math->text().at(i).isDigit())
            {
            all_digit=false;
            break;
            }
        }
    }
    if(all_digit)
    {
    for(size_t i=0;i<eng_length;i++)
    {

        if(!ui->stu_english->text().at(i).isDigit())
        {
            all_digit=false;
            break;
        }
    }
    }
    if(all_digit)
    {
        for(size_t i=0;i<com_length;i++)
        {

            if(!ui->stu_comprehension->text().at(i).isDigit())
            {
            all_digit=false;
            break;
            }
        }
    }
    if(!all_digit)
    {
        QMessageBox::warning(this,"输入提示","输入有误！");
        return;
    }//异常处理1：输入分数非数字
    unsigned chinese=ui->stu_chinese->text().toUInt();
    unsigned math=ui->stu_math->text().toUInt();
    unsigned english=ui->stu_english->text().toUInt();
    unsigned comprehension=ui->stu_comprehension->text().toUInt();

    if(chinese>150||math>150||english>150||comprehension>300)
    {
        QMessageBox::warning(this,"输入提示","输入有误！");
        return;
    }//异常处理2：输入分数超标

    //在此处将读取的数据写入数据库
    Student stu(name,id,chinese,math,english,comprehension);
    TCPClient tcp;
    if(tcp.myupdate(stu))
    {
        QMessageBox::information(this,"修改提示","修改成功！");
    }
 else
    {
        QMessageBox::warning(this,"修改提示","修改失败！");
    }


    //数据库修改的异常处理
    //codes here...

    //如果修改成功了



}


    //数据删除的操作


void Modify::on_deleteButton_clicked()
{
    QString deleteID=ui->deleteName->text();
    TCPClient tcp;
    if(tcp.mydelete(deleteID))
    {
        QMessageBox::information(this,"删除提示","删除成功！");
    }
 else
    {
        QMessageBox::warning(this,"删除提示","删除失败！");
    }
}


void Modify::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawPixmap(0,0,width(),height(),QPixmap(BACKGROUND_PATH));
}
