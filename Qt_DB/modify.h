#ifndef MODIFY_H
#define MODIFY_H

#include <QWidget>
#include <QMessageBox>
#include <QString>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QPainter>
#include<connect.h>
#include "picture.h"
namespace Ui {
class Modify;
}

class Modify : public QWidget
{
    Q_OBJECT

public:
    explicit Modify(QWidget *parent = nullptr);
    ~Modify();

signals:
    void backMenu();

private slots:
    void on_backButton_clicked();

    void on_modifyButton_clicked();

    void on_deleteButton_clicked();

private:
    Ui::Modify *ui;
     virtual void paintEvent(QPaintEvent *event);
    //QSqlDatabase db;
};

#endif // MODIFY_H
